# design-patterns

# The Gang of Four
https://springframework.guru/gang-of-four-design-patterns/

# Books
1. The art of readable code
2. Head first Design Patterns
3. Clean code
4. Effective Java