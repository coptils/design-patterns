package com.sda.fundamentals.creational.builder;

public class Pizza {

    private String name;
    private int size;
    private boolean isAddCheese;
    private boolean isAddSauce;
    private boolean isTakeaway;

    public Pizza(PizzaBuilder builder) {
        this.name = builder.name;
        this.size = builder.size;
        this.isAddCheese = builder.isAddCheese;
        this.isAddSauce = builder.isAddSauce;
        this.isTakeaway = builder.isTakeaway;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public boolean isAddCheese() {
        return isAddCheese;
    }

    public boolean isAddSauce() {
        return isAddSauce;
    }

    public boolean isTakeaway() {
        return isTakeaway;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", size=" + size +
                ", add cheese=" + isAddCheese +
                ", add sauce=" + isAddSauce +
                ", takeaway=" + isTakeaway +
                '}';
    }

    public static class PizzaBuilder {
        private String name;
        private int size;
        private boolean isAddCheese;
        private boolean isAddSauce;
        private boolean isTakeaway;

        public PizzaBuilder() {
        }

        public PizzaBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public PizzaBuilder withSize(int size) {
            this.size = size;
            return this;
        }

        public PizzaBuilder addChees(boolean isAddChees) {
            this.isAddCheese = isAddChees;
            return this;
        }

        public PizzaBuilder addSauce(boolean isAddSauce) {
            this.isAddSauce = isAddSauce;
            return this;
        }

        public PizzaBuilder takeaway(boolean isTakeaway) {
            this.isTakeaway = isTakeaway;
            return this;
        }

        public Pizza build() {
            return new Pizza(this);
        }

    }

}
