package com.sda.fundamentals.creational.builder;

public class PizzaBuilderTest {
    public static void main(String[] args) {
//         Pizza margharita = new Pizza("Margharita", 500, null, true, true);
        Pizza pizza = new Pizza.PizzaBuilder()
                .withName("Margharita")
                .withSize(20)
                .takeaway(true)
                .addSauce(true)
                .addChees(true)
                .build();
        System.out.println(pizza);
    }
}
