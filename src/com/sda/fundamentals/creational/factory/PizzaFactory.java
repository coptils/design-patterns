package com.sda.fundamentals.creational.factory;

public class PizzaFactory {

    public static Pizza getPizza(int number) {
        Pizza pizza = null;
        switch (number) {
            case 1:
                pizza = new Capriciosa();
                break;
            case 2:
                pizza = new Margharita();
                break;
        }
        return pizza;
    }

}
