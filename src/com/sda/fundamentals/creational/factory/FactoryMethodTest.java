package com.sda.fundamentals.creational.factory;

public class FactoryMethodTest {
    public static void main(String[] args) {
        Pizza pizza = PizzaFactory.getPizza(1);
        System.out.println(pizza.getDetectedPizza());
    }
}
