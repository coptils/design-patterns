package com.sda.fundamentals.creational.factory;

public class Capriciosa implements Pizza {
    @Override
    public String getDetectedPizza() {
        return "1.Capriciosa";
    }
}
