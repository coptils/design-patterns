package com.sda.fundamentals.creational.factory;

public interface Pizza {
    String getDetectedPizza();
}
