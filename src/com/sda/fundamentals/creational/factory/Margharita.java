package com.sda.fundamentals.creational.factory;

public class Margharita implements Pizza {
    @Override
    public String getDetectedPizza() {
        return "2.Margharita";
    }
}
