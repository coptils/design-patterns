package com.sda.fundamentals.creational.exercises.factorymethod;

public interface Shape {
   void draw();
}