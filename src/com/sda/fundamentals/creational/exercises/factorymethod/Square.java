package com.sda.fundamentals.creational.exercises.factorymethod;

public class Square implements Shape {

   @Override
   public void draw() {
      System.out.println("Inside Square::draw() method.");
   }
}