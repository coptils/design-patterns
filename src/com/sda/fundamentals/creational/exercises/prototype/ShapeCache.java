package com.sda.fundamentals.creational.exercises.prototype;

import com.sda.fundamentals.creational.exercises.prototype.shape.Circle;
import com.sda.fundamentals.creational.exercises.prototype.shape.Rectangle;
import com.sda.fundamentals.creational.exercises.prototype.shape.Shape;
import com.sda.fundamentals.creational.exercises.prototype.shape.Square;

import java.util.Hashtable;

public class ShapeCache {
	
   private static Hashtable<String, Shape> cache = new Hashtable<String, Shape>();

   public static Shape getShape(String shapeId) {
      Shape cachedShape = cache.get(shapeId);
      return (Shape) cachedShape.clone();
   }

   // for each shape run database query and create shape
   // shapeMap.put(shapeKey, shape);
   // for example, we are adding three shapes
   
   public static void loadCache() {
      Circle circle = new Circle();
      circle.setId("1");
      cache.put(circle.getId(), circle);

      Square square = new Square();
      square.setId("2");
      cache.put(square.getId(), square);

      Rectangle rectangle = new Rectangle();
      rectangle.setId("3");
      cache.put(rectangle.getId(), rectangle);
   }
}