package com.sda.fundamentals.creational.exercises.builder;

//1. Create new Block class with the name, shape, color, penWidth properties.
//2. Create builder class using previously created class.
public class Block {

    private String name;
    private String shape;
    private String color;
    private int penWidth;

    public Block(BlockBuilder builder) {
        this.name = builder.name;
        this.shape = builder.shape;
        this.color = builder.color;
        this.penWidth = builder.penWidth;
    }

    @Override
    public String toString() {
        return "Block{" +
                "name='" + name + '\'' +
                ", shape='" + shape + '\'' +
                ", color='" + color + '\'' +
                ", penWidth=" + penWidth +
                '}';
    }

    public static class BlockBuilder {
        private String name;
        private String shape;
        private String color;
        private int penWidth;

        public BlockBuilder(String name) {
            this.name = name;
        }

        public BlockBuilder withShape(String shape) {
            this.shape = shape;
            return this;
        }

        public BlockBuilder withColor(String color) {
            this.color = color;
            return this;
        }

        public BlockBuilder withPenWidth(int penWidth) {
            this.penWidth = penWidth;
            return this;
        }

        public Block build() {
            return new Block(this);
        }
    }

}
