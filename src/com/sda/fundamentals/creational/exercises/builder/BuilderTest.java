package com.sda.fundamentals.creational.exercises.builder;

public class BuilderTest {
    public static void main(String[] args) {
        Block block = new Block.BlockBuilder("Square")
                .withColor("red")
                .withShape("square")
                .withPenWidth(20)
                .build();
    }
}
