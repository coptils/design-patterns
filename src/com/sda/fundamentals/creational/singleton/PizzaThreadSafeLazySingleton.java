package com.sda.fundamentals.creational.singleton;

public class PizzaThreadSafeLazySingleton {

    private static PizzaThreadSafeLazySingleton instance = null;

    public String name;

    // in order to make this thread safe, we should use synchronized keyword
    public synchronized static PizzaThreadSafeLazySingleton getInstance() {
        if (instance == null) {
            instance = new PizzaThreadSafeLazySingleton();
        }
        return instance;
    }

    private PizzaThreadSafeLazySingleton() {
        name = "Capriciosa";
    }

    @Override
    public String toString() {
        return "Pizza name: " + name;
    }
}
