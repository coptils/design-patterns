package com.sda.fundamentals.creational.singleton;

public class TestSingleton {
    public static void main(String[] args) {
        PizzaLazySingleton pizza = PizzaLazySingleton.getInstance(); // o sa returneze instanta din clasa Pizza
        System.out.println(pizza);
        pizza.name = "My Custom Pizza";
        System.out.println(pizza);
        pizza.name = "Margharita";
        System.out.println(pizza);
        System.out.println(PizzaLazySingleton.getInstance());

        PizzaLazySingleton pizza1 = PizzaLazySingleton.getInstance();
        PizzaLazySingleton pizza2 = PizzaLazySingleton.getInstance();
        pizza2.name = "Custom Pizza";
        PizzaLazySingleton pizza3 = PizzaLazySingleton.getInstance();
        pizza3.name = "Latest Name of Pizza";

        System.out.println(pizza);
        System.out.println(pizza1);
        System.out.println(pizza2);
        System.out.println(pizza3);
    }
}
