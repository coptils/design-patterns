package com.sda.fundamentals.creational.singleton;

// This will impact your startup time performance, but it may help improve the runtime performance of your application
// thread safe
public class PizzaEagerSingleton {

    private static final PizzaEagerSingleton INSTANCE = new PizzaEagerSingleton(); // always we will have an instance in application, even if we don't need it

    public String name;

    // o sa ne returneze tot timpul aceeasi instanta a clasei
    public static PizzaEagerSingleton getInstance() {
        return INSTANCE;
    }

    // pentru Singleton, nu avem constructor public
    private PizzaEagerSingleton() {
        name = "Capriciosa";
    }

    @Override
    public String toString() {
        return "Pizza name: " + name;
    }
}
