package com.sda.fundamentals.creational.singleton;

// not thread safe
public class PizzaLazySingleton {

    private static PizzaLazySingleton instance = null; // instanta o sa se creeze doar la cerere

    public String name;

    // o sa ne returneze tot timpul aceeasi instanta a clasei
    public static PizzaLazySingleton getInstance() {
        if (instance == null) {
            instance = new PizzaLazySingleton();
        }
        return instance;
    }

    // pentru Singleton, nu avem constructor public
    private PizzaLazySingleton() {
        name = "Capriciosa";
    }

    @Override
    public String toString() {
        return "Pizza name: " + name;
    }
}
