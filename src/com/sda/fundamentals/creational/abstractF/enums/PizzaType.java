package com.sda.fundamentals.creational.abstractF.enums;

public enum PizzaType {
    Capriciosa,
    Margharita
}
