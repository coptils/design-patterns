package com.sda.fundamentals.creational.abstractF.pizza;

public class Capriciosa extends Pizza {

    private int size;

    public Capriciosa(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return "Capriciosa";
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public String getIngredients() {
        return "Cheese, Tomato Sauce, Ham, Mushrooms";
    }
}
