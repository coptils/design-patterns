package com.sda.fundamentals.creational.abstractF.factories;

import com.sda.fundamentals.creational.abstractF.pizza.Margharita;
import com.sda.fundamentals.creational.abstractF.pizza.Pizza;

public class MargharitaFactory implements PizzaAbstractFactory {
    @Override
    public Pizza create(int size) {
        return new Margharita(size);
    }
}
