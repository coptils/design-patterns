package com.sda.fundamentals.creational.abstractF.factories;


import com.sda.fundamentals.creational.abstractF.enums.PizzaType;
import com.sda.fundamentals.creational.abstractF.pizza.Pizza;

public class PizzaFactory {

    public static Pizza createPizza(PizzaType type, int size) {
        Pizza pizza = null;
        switch (type) {
            case Capriciosa:
                pizza = new CapriciosaFactory().create(size);
                break;
            case Margharita:
                pizza = new MargharitaFactory().create(size);
                break;
        }

        return pizza;
    }


}
