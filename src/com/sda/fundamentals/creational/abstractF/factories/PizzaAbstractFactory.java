package com.sda.fundamentals.creational.abstractF.factories;

import com.sda.fundamentals.creational.abstractF.pizza.Pizza;

public interface PizzaAbstractFactory {

    Pizza create(int size);

}
