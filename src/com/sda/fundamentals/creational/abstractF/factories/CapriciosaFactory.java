package com.sda.fundamentals.creational.abstractF.factories;

import com.sda.fundamentals.creational.abstractF.pizza.Capriciosa;
import com.sda.fundamentals.creational.abstractF.pizza.Pizza;

public class CapriciosaFactory implements PizzaAbstractFactory {

    @Override
    public Pizza create(int size) {
        return new Capriciosa(size);
    }
}
