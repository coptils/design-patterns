package com.sda.fundamentals.creational.abstractF;

import com.sda.fundamentals.creational.abstractF.enums.PizzaType;
import com.sda.fundamentals.creational.abstractF.factories.PizzaFactory;
import com.sda.fundamentals.creational.abstractF.pizza.Pizza;

import static java.lang.Math.PI;

public class AbstractFactory {
    public static void main(String[] args) {
        double ss =PI;
        Pizza margharita = PizzaFactory.createPizza(PizzaType.Capriciosa, 500);
        Pizza capriciosa = PizzaFactory.createPizza(PizzaType.Margharita, 350);
        System.out.println(margharita);
        System.out.println(capriciosa);
    }
}
