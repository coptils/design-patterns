package com.sda.fundamentals.creational.prototype;

public class PineTree extends Tree {

    private int height;
    private int mass;
    private String position;

    public PineTree(int mass, int height) {
        this.height = height;
        this.mass = mass;
    }

    @Override
    public Tree copy() {
        PineTree pineTreeClone = new PineTree(this.getMass(), this.getHeight());
        pineTreeClone.setPosition(this.getPosition());
        return pineTreeClone;
    }

    public int getHeight() {
        return height;
    }

    public int getMass() {
        return mass;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}