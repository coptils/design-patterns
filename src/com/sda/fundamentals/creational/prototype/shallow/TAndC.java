package com.sda.fundamentals.creational.prototype.shallow;

import java.util.Optional;

public class TAndC extends PrototypeCapableDocument {
    @Override
    public Optional<PrototypeCapableDocument> cloneDocument() {
        /*Clone with shallow copy*/
        try {
            return Optional.ofNullable((TAndC) super.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public String toString() {
        return "[TAndC: Vendor Name - " + getVendorName() + ", Content - " + getContent() + "]";
    }
}