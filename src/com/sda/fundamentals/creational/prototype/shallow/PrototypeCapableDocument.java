package com.sda.fundamentals.creational.prototype.shallow;

import java.util.Optional;

public abstract class PrototypeCapableDocument implements Cloneable {
    private String vendorName;
    private String content;

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public abstract Optional<PrototypeCapableDocument> cloneDocument() throws CloneNotSupportedException;
}