package com.sda.fundamentals.creational.prototype;

public abstract class Tree implements Cloneable {

    public abstract Tree copy();
    
}