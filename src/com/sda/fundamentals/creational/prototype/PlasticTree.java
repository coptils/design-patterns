package com.sda.fundamentals.creational.prototype;

public class PlasticTree extends Tree {

    private int height;
    private int mass;
    private String position;

    public PlasticTree(int mass, int height) {
        this.height = height;
        this.mass = mass;
    }

    @Override
    // deep copy - se face un obiect nou care are o referinta noua in memorie
    public Tree copy() {
        PlasticTree plasticTreeClone = new PlasticTree(this.getMass(), this.getHeight());
        plasticTreeClone.setPosition(this.getPosition());
        return plasticTreeClone;
    }

    public int getHeight() {
        return height;
    }

    public int getMass() {
        return mass;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}