package com.sda.fundamentals.fluentinterface;

public interface Pizza {

    Pizza getName();

    Pizza getIngredients();

    Integer getCost();
}
