package com.sda.fundamentals.fluentinterface;

public interface Restaurant {

    Restaurant getName(String name);

    Menu getMenu();
}
