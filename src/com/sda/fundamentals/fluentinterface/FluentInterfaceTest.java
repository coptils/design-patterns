package com.sda.fundamentals.fluentinterface;

import java.util.Arrays;

public class FluentInterfaceTest {
    public static void main(String[] args) {
        PizzaRestaurant pizzaRestaurant = new PizzaRestaurant();
        pizzaRestaurant.name = "Primavera";

        new PizzaRestaurant()
                .getName("Primavera")
                .getMenu()
                .searchPizza()
                .orderPizza(Arrays.asList(new Integer[]{1, 3}))
                .eatPizza()
                .payPizza()
                .getPizza(1)
                .getCost();
    }

}
