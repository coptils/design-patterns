package com.sda.fundamentals.fluentinterface;

import java.util.List;

public interface Menu {

    Menu searchPizza();

    Menu orderPizza(List<Integer> orders);

    Menu eatPizza();

    Menu payPizza();

    Pizza getPizza(int index);
}
