package com.sda.fundamentals.structural.decorator.pizza;

public class PizzaDecorator implements Pizza {

    protected Pizza pizza;

    public PizzaDecorator(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public void printIngredients() {
        this.pizza.printIngredients();
    }

    @Override
    public void addIngredients(String ingredient) {
        this.pizza.addIngredients(ingredient);
    }
}
