package com.sda.fundamentals.structural.decorator.pizza;

public interface Pizza {

    void addIngredients(String ingredient);

    void printIngredients();

}
