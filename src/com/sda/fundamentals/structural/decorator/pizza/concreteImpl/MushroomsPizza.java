package com.sda.fundamentals.structural.decorator.pizza.concreteImpl;

import com.sda.fundamentals.structural.decorator.pizza.Pizza;
import com.sda.fundamentals.structural.decorator.pizza.PizzaDecorator;

public class MushroomsPizza extends PizzaDecorator {

    public MushroomsPizza(Pizza pizza) {
        super(pizza);
        super.addIngredients("Mushrooms");
    }

    @Override
    public void printIngredients() {
        super.printIngredients();
    }
}
