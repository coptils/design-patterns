package com.sda.fundamentals.structural.decorator.pizza.concreteImpl;

import com.sda.fundamentals.structural.decorator.pizza.Pizza;
import com.sda.fundamentals.structural.decorator.pizza.PizzaDecorator;

public class SeafoodPizza extends PizzaDecorator {

    public SeafoodPizza(Pizza pizza) {
        super(pizza);
        super.addIngredients("Seafood");
    }

    @Override
    public void printIngredients() {
        super.printIngredients();
    }
}
