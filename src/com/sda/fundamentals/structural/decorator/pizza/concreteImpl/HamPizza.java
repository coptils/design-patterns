package com.sda.fundamentals.structural.decorator.pizza.concreteImpl;

import com.sda.fundamentals.structural.decorator.pizza.Pizza;
import com.sda.fundamentals.structural.decorator.pizza.PizzaDecorator;

public class HamPizza extends PizzaDecorator {

    public HamPizza(Pizza pizza) {
        super(pizza);
        super.addIngredients("Ham");
    }

    @Override
    public void printIngredients() {
        super.printIngredients();
    }
}
