package com.sda.fundamentals.structural.decorator;

import com.sda.fundamentals.structural.decorator.pizza.BasicPizza;
import com.sda.fundamentals.structural.decorator.pizza.Pizza;
import com.sda.fundamentals.structural.decorator.pizza.concreteImpl.HamPizza;
import com.sda.fundamentals.structural.decorator.pizza.concreteImpl.MushroomsPizza;
import com.sda.fundamentals.structural.decorator.pizza.concreteImpl.SeafoodPizza;

/**
 * Decoratorul - adauga responsabilitati aditionale unui obiect de baza, in mod dinamic prin crearea altor obiecte
 */
public class DecoratorTest {

    public static void main(String[] args) {
        Pizza pizza = new BasicPizza(); // obiectul pe care vrem sa il decoram
        pizza.printIngredients();

        Pizza hamPizza = new HamPizza(pizza);
        // afiseaza ingredientele initiale + cele introduse de catre decorator
        pizza.printIngredients();

        Pizza mushroomsPizza = new MushroomsPizza(pizza);
        // afiseaza ingredientele initiale + cele introduse de catre decorator la linia 9 + cele introduse la linia 13
        pizza.printIngredients();

        Pizza seafoodPizza = new SeafoodPizza(pizza);
        pizza.printIngredients();
    }

}
