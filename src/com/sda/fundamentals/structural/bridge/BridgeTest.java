package com.sda.fundamentals.structural.bridge;

import com.sda.fundamentals.structural.bridge.message.EmailMessage;
import com.sda.fundamentals.structural.bridge.message.Message;
import com.sda.fundamentals.structural.bridge.message.TextMessage;
import com.sda.fundamentals.structural.bridge.sender.EmailMessageSender;
import com.sda.fundamentals.structural.bridge.sender.MessageSender;
import com.sda.fundamentals.structural.bridge.sender.TextMessageSender;

/**
 * Decouple an abstraction from its implementation so that the two can vary independently.
 */
public class BridgeTest {

    public static void main(String[] args) {
        MessageSender textMessageSender = new TextMessageSender();
        Message textMessage = new TextMessage(textMessageSender);

        textMessage.send();

        MessageSender emailMessageSender = new EmailMessageSender();
        Message emailMessage = new EmailMessage(emailMessageSender);
        emailMessage.send();
    }
}