package com.sda.fundamentals.structural.bridge.message;

import com.sda.fundamentals.structural.bridge.sender.MessageSender;

//Abstraction (Message): Is the interface implemented as an abstract class that clients communicate with.
public abstract class Message {

    // ------ aici stabilim bridge-ul intre cele doua ierarhii de clase
    // has-A => functie de agregare <=> composition
    MessageSender messageSender;

    public Message(MessageSender messageSender) {
        this.messageSender = messageSender;
    }
    // --------

    public abstract void send();
}