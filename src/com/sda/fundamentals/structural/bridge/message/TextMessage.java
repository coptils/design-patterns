package com.sda.fundamentals.structural.bridge.message;

import com.sda.fundamentals.structural.bridge.sender.MessageSender;

// RefinedAbstraction (TextMessage and EmailMessage): Are classes that implement or extend Abstraction.
public class TextMessage extends Message {

    public TextMessage(MessageSender messageSender) {
        super(messageSender);
    }

    @Override
    public void send() {
      messageSender.sendMessage();
    }
}