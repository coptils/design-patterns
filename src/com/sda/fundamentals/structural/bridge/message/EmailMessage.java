package com.sda.fundamentals.structural.bridge.message;

import com.sda.fundamentals.structural.bridge.sender.MessageSender;

//RefinedAbstraction (TextMessage and EmailMessage): Are classes that implement or extend Abstraction.
public class EmailMessage extends Message{

    public EmailMessage(MessageSender messageSender) {
        super(messageSender);
    }

    @Override
    public void send(){
        messageSender.sendMessage();
    }
}