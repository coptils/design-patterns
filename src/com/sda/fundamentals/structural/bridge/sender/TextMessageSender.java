package com.sda.fundamentals.structural.bridge.sender;

//ConcreteImplementor(TextMessageSender and EmailMessageSender): Are concrete subclasses that implements Implementor.
public class TextMessageSender implements MessageSender {
    @Override
    public void sendMessage() {
        System.out.println("TextMessageSender: Sending text message...");
    }
}
