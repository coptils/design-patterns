package com.sda.fundamentals.structural.bridge.sender;

//Implementor (MessageSender): Is the interface of the implementation class hierarchy.
// aceasta ierarhie de clase este complet independenta de abstractia ierarhiei de clase din packetul message
// => putem sa modificam, sa extindem si sa refolosim o ierarhie de clase fara sa ne ingrijoram de structura participantilor din cealalta ierarhie de clase
public interface MessageSender {

    void sendMessage();

}