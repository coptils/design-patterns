package com.sda.fundamentals.structural.facade;

import com.sda.fundamentals.structural.facade.controller.OrderFulfillmentController;
import com.sda.fundamentals.structural.facade.servicefacade.OrderServiceFacadeImpl;

public class FacadeTest {
    public static void main(String[] args) {
        OrderFulfillmentController controller = new OrderFulfillmentController();
        controller.facade = new OrderServiceFacadeImpl();

        controller.orderProduct(9);
        boolean result = controller.orderFulfilled;
        System.out.println(result);
    }
}
