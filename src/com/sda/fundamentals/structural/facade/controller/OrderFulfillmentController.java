package com.sda.fundamentals.structural.facade.controller;

import com.sda.fundamentals.structural.facade.servicefacade.OrderServiceFacade;

public class OrderFulfillmentController {

    public OrderServiceFacade facade; // composite
//
//    InventoryService inventoryService;
//    PaymentService paymentService;
//    ShippingService shippingService;

    public boolean orderFulfilled = false;

    public void orderProduct(int productId) {
//        orderFulfilled = facade.placeOrder(productId);

        if(facade.isProductAvailable(productId) && facade.isPaymentConfirmed()) {
            facade.shipProduct(productId);
        }

        System.out.println("OrderFulfillmentController: Order fulfillment completed. ");
    }
}