package com.sda.fundamentals.structural.facade.servicefacade;

import com.sda.fundamentals.structural.facade.domain.Product;
import com.sda.fundamentals.structural.facade.subcomponents.InventoryService;
import com.sda.fundamentals.structural.facade.subcomponents.PaymentService;
import com.sda.fundamentals.structural.facade.subcomponents.ShippingService;

public class OrderServiceFacadeImpl implements OrderServiceFacade{

//    public boolean placeOrder(int pId){
//        boolean orderFulfilled = false;
//        Product product = new Product();
//        product.productId=pId;
//
//        if(isProductAvailable(pId))
//        {
//            System.out.println("Product with ID: "+ product.productId+" is available.");
//            if(isPaymentConfirmed()){
//                System.out.println("Payment confirmed...");
//                shipProduct(pId);
//                System.out.println("Product shipped...");
//                orderFulfilled=true;
//            }
//        }
//        return orderFulfilled;
//    }

    @Override
    public boolean isProductAvailable(int productId) {
        Product product = new Product();
        product.productId = productId;
        return InventoryService.isAvailable(product);
    }

    @Override
    public boolean isPaymentConfirmed() {
        return PaymentService.makePayment();
    }

    @Override
    public void shipProduct(int productId) {
        Product product = new Product();
        product.productId=productId;
        ShippingService.shipProduct(product);
    }
}