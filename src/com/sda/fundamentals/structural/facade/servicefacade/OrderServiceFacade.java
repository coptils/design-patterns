package com.sda.fundamentals.structural.facade.servicefacade;

public interface OrderServiceFacade {
    boolean isProductAvailable(int productId);
    boolean isPaymentConfirmed();
    void shipProduct(int productId);
//    boolean placeOrder(int productId);
}