package com.sda.fundamentals.structural.facade.subcomponents;

import com.sda.fundamentals.structural.facade.domain.Product;

public class ShippingService {
    public static void shipProduct(Product product) {
        /*Connect with external shipment service to ship product*/
    }
}
