package com.sda.fundamentals.structural.composite;

// Leaf(Product): Is a class that extends Component to represent leaves in the tree structure that does not have any children.
public class Product extends CatalogComponent {
    private final String name;
    private final double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public void print() {
        System.out.println("Product name: " + name + " Price: " + price);
    }
}