package com.sda.fundamentals.structural.composite;

import java.util.ArrayList;

//Composite (ProductCatalog): Is a class that extends Component to represent nodes (contain children) in the tree structure.
// This class stores Leaf components and implements the behaviors defined in Component to access and manage child components.
// As mentioned earlier, child components can be one or more Leaf or other Composite components.
public class ProductCatalog extends CatalogComponent {

    // functia de agregare intre Composite (nod intermediar) si Component (interfata comuna cu clientul)
    private final ArrayList<CatalogComponent> items = new ArrayList<>();
    private final String name;

    public ProductCatalog(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void print() {
        for (CatalogComponent comp : items) {
            comp.print();
        }
    }

    @Override
    public void add(CatalogComponent catalogComponent) {
        items.add(catalogComponent);
    }

    @Override
    public void remove(CatalogComponent catalogComponent) {
        items.remove(catalogComponent);
    }
}