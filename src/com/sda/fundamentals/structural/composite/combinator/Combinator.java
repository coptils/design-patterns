package com.sda.fundamentals.structural.composite.combinator;

import java.util.function.Function;

// combine primitives into more complex structures
public class Combinator {

    public static void main(String[] args) {

        Function<String, String> f1 = new Function<String, String>() {
            @Override
            public String apply(String s) {
                return s.replace("a", "b");
            }
        };

        String input = "aaa,ccc";
        String result = f1.apply(input);
        System.out.println(result);

        Function<String, String> f2 = (s) -> {
            return s.replace("a", "b");
        };

        Function<Double, Integer> f5 = no -> 2+23;

        System.out.println(f1.apply(input));

        Function<String, String> f3 = s -> s.replace("a", "b");
        System.out.println(f1.apply(input));

        Function<String, String> replaceCommasWithDotsFunction = s -> s.replace(",", ".");
        Function<String, String> replaceDotsWithSpaceFunction = s -> s.replace(".", " ");
        Function<String, String> replaceSpaceWithStarFunction = s -> s.replace(" ", "*");

        Function<String, String> function = replaceCommasWithDotsFunction // base function
                .compose(replaceDotsWithSpaceFunction)  // node
                .andThen(replaceSpaceWithStarFunction); // leaf

//        System.out.println(function.apply("a,b,c"));
        System.out.println(function.apply("a b c"));
    }

}
