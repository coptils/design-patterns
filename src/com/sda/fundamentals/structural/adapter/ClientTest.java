package com.sda.fundamentals.structural.adapter;

import com.sda.fundamentals.structural.adapter.adaptee.CsvFormattable;
import com.sda.fundamentals.structural.adapter.adaptee.CsvFormatter;
import com.sda.fundamentals.structural.adapter.adapter.CsvAdapterImpl;
import com.sda.fundamentals.structural.adapter.target.NewLineFormatter;
import com.sda.fundamentals.structural.adapter.target.TextFormattable;

//Client: Communicates with the Target.
public class ClientTest {
    public static void main(String[] args) {
        // textul ce trebuie formatat
        String testString=" Formatting line 1. Formatting line 2. Formatting line 3.";

        TextFormattable newLineFormatter=new NewLineFormatter();
        String resultString = newLineFormatter.formatText(testString);
        System.out.println(resultString);

        CsvFormattable csvFormatter=new CsvFormatter();
        TextFormattable csvAdapter=new CsvAdapterImpl(csvFormatter);
        String resultCsvString=csvAdapter.formatText(testString);
        System.out.println(resultCsvString);
    }
}