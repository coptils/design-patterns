package com.sda.fundamentals.structural.adapter.adapter;

import com.sda.fundamentals.structural.adapter.adaptee.CsvFormattable;
import com.sda.fundamentals.structural.adapter.target.TextFormattable;

//Adapter (CsvAdapterImpl): A class that adapts the Adaptee to the Target.
public class CsvAdapterImpl implements TextFormattable {

    // adaptee = ce vrem sa adaptam la target
    CsvFormattable csvFormatter;

    public CsvAdapterImpl(CsvFormattable csvFormatter){
        this.csvFormatter=csvFormatter;
    }

    @Override
    public String formatText(String text) {
        return csvFormatter.formatCsvText(text);
    }

}