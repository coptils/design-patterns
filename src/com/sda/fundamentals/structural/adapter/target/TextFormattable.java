package com.sda.fundamentals.structural.adapter.target;

//Target (TextFormattable): The existing interface that clients communicate with.
public interface TextFormattable {
    String formatText(String text);
}
