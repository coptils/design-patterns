package com.sda.fundamentals.structural.adapter.target;

public class NewLineFormatter implements TextFormattable {
    @Override
    public String formatText(String text) {
        return text.replace(".","\n");
    }
}