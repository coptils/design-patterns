package com.sda.fundamentals.structural.adapter.adaptee;

//Adaptee (CsvFormattable): The new incompatible interface that needs adapting.
public interface CsvFormattable { //"str1, str2, str3, str4, ..." csv => comma separated values (valori separate prin virgula)
    // csv = comma separated value
    String formatCsvText(String text);
}
