package com.sda.fundamentals.structural.adapter.adaptee;

public class CsvFormatter implements CsvFormattable {
    @Override
    public String formatCsvText(String text){
//        "Ana. Maria. Iulia." => "Ana, Maria, Iulia,"
        String formattedText=text.replace(".",",");
        return formattedText;
    }
}