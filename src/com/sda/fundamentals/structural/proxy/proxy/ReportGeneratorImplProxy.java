package com.sda.fundamentals.structural.proxy.proxy;

import com.sda.fundamentals.structural.proxy.Role;
import com.sda.fundamentals.structural.proxy.realsubject.ReportGeneratorImpl;
import com.sda.fundamentals.structural.proxy.subject.ReportGenerator;

// Proxy
public class ReportGeneratorImplProxy implements ReportGenerator {

    private final Role accessRole;

    // mentine o referinta la obiectul real
    ReportGenerator reportGeneratorImpl;

    public ReportGeneratorImplProxy(Role accessRole) {
        this.accessRole = accessRole;
    }

    @Override
    public void displayReportTemplate(String reportFormat, int reportEntries) {
        if (reportGeneratorImpl == null) {
            reportGeneratorImpl = new ReportGeneratorImpl(); // lazy instantiation
        }
        reportGeneratorImpl.displayReportTemplate(reportFormat, reportEntries);
    }

    @Override
    public void generateComplexReport(String reportFormat, int reportEntries) {
        if (reportGeneratorImpl == null) {
            reportGeneratorImpl = new ReportGeneratorImpl(); // lazy instantiation
        }
        reportGeneratorImpl.generateComplexReport(reportFormat, reportEntries); // apel la operatia de pe obiectul real
    }

    @Override
    public void generateSensitiveReport() {
        if (accessRole.getRole().equals("Manager")) { // protection proxy -> controls access to the real object
            if (reportGeneratorImpl == null) {
                reportGeneratorImpl = new ReportGeneratorImpl();
            }
            reportGeneratorImpl.generateSensitiveReport(); // apel la operatia de pe obiectul real
        } else {
            System.out.println("You are not authorized to access sensitive reports.");
        }
    }
}