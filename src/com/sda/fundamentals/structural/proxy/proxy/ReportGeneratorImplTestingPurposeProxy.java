package com.sda.fundamentals.structural.proxy.proxy;

import com.sda.fundamentals.structural.proxy.subject.ReportGenerator;

// Proxy
public class ReportGeneratorImplTestingPurposeProxy implements ReportGenerator {

    // mentine o referinta la obiectul real
    ReportGenerator reportGeneratorImpl;

    public ReportGeneratorImplTestingPurposeProxy() {
    }

    @Override
    public void displayReportTemplate(String reportFormat, int reportEntries) {
        System.out.println("this is your report template");
    }

    @Override
    public void generateComplexReport(String reportFormat, int reportEntries) {
        System.out.println("Your complexe report was generated");
    }

    @Override
    public void generateSensitiveReport() {
        System.out.println("Your report was generated.");
    }
}