package com.sda.fundamentals.structural.proxy.subject;

// Subject
// este o interfata atat pentru obiectul real cat si pentru proxy
// aceasta interfata permite ca proxy-ul sa fie folosit oriunde este nevoie de obiectul real
public interface ReportGenerator {
    void displayReportTemplate(String reportFormat, int reportEntries);

    void generateComplexReport(String reportFormat, int reportEntries);

    void generateSensitiveReport();
}