package com.sda.fundamentals.structural.proxy.realsubject;

import com.sda.fundamentals.structural.proxy.subject.ReportGenerator;

// RealSubject -> obiectul real care este costisitor de creat, are nevoie de protectie sau ruleaza pe un JVM remote
// o sa ii creem un proxy
public class ReportGeneratorImpl implements ReportGenerator {
    public ReportGeneratorImpl() {
        System.out.println("ReportGeneratorImpl instance created");
    }

    @Override
    public void displayReportTemplate(String reportFormat, int reportEntries) {
        System.out.println("ReportGeneratorImpl: Displaying blank report template in "
                + reportFormat + " format with " + reportEntries + " entries");
    }

    @Override
    public void generateComplexReport(String reportFormat, int reportEntries) {
        System.out.println("ReportGeneratorImpl: Generating complex report in " + reportFormat + " format with " + reportEntries + " entries");
    }

    @Override
    public void generateSensitiveReport() {
        System.out.println("ReportGeneratorImpl: Generating sensitive report");
    }
}