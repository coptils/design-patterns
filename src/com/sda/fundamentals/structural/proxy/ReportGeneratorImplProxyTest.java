package com.sda.fundamentals.structural.proxy;

import com.sda.fundamentals.structural.proxy.proxy.ReportGeneratorImplProxy;
import com.sda.fundamentals.structural.proxy.subject.ReportGenerator;

public class ReportGeneratorImplProxyTest {
    public static void main(String[] args) {
        Role accessRole = new Role();
        accessRole.setRole("Angajat");

        ReportGenerator proxy = new ReportGeneratorImplProxy(accessRole);
        proxy.displayReportTemplate("Pdf", 150);
        proxy.generateComplexReport("Pdf", 150);
        proxy.generateSensitiveReport();
    }
}