package com.sda.fundamentals.structural.flyweight.flyweightobjects;

// Flyweight => clasa abstracta / interfata pentru obiectele flyweight
// declara metode prin care flyweights poate actiona pe starile extrinseci
public abstract class RaceCar {
    /*Intrinsic state  stored and shared in the Flyweight object*/
    // sharable
    public String name; // numele masinii
    public int speed; // viteza
    public int horsePower; // puterea motorului

    /* Extrinsic state is stored or computed by client objects, and passed to the Flyweight.*/
    // cannot be shared
    public abstract void moveCar(int currentX, int currentY, int newX, int newY);
}