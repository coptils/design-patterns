package com.sda.fundamentals.structural.flyweight.client;

import com.sda.fundamentals.structural.flyweight.flyweightfactory.CarFactory;
import com.sda.fundamentals.structural.flyweight.flyweightobjects.RaceCar;

// Client
// cere un FlyweightFactory pentru un obiect flyweight, si apoi calculeaza si paseaza datele extrinseci
public class RaceCarClient {

    private final RaceCar raceCar;
    /**
     * The extrinsic state of the flyweight is maintained by the client
     */
    private int currentX = 0;
    private int currentY = 0;

    public RaceCarClient(String name) {
        /*Ask factory for a RaceCar*/
        raceCar = CarFactory.getRaceCar(name);
    }

    public void moveCar(int newX, int newY) {
        /*Car movement is handled by the flyweight object*/
        raceCar.moveCar(currentX, currentY, newX, newY);
        currentX = newX;
        currentY = newY;
    }
}