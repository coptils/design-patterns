package com.sda.fundamentals.structural.exercises.bridge.abstraction;

import com.sda.fundamentals.structural.exercises.bridge.implementation.Workshop;

public abstract class Vehicle {
    protected Workshop workShop1;
    protected Workshop workShop2;

    protected Vehicle(Workshop workShop1, Workshop workShop2) {
        this.workShop1 = workShop1;
        this.workShop2 = workShop2;
    }

    public abstract void manufacture();
}