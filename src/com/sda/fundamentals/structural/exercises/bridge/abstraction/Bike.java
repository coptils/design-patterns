package com.sda.fundamentals.structural.exercises.bridge.abstraction;

import com.sda.fundamentals.structural.exercises.bridge.implementation.Workshop;

public class Bike extends Vehicle {
    public Bike(Workshop workShop1, Workshop workShop2) {
        super(workShop1, workShop2);
    }

    @Override
    public void manufacture() {
        System.out.print("Bike ");
        workShop1.work();
        workShop2.work();
    }
}