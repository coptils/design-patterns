package com.sda.fundamentals.structural.exercises.bridge;

import com.sda.fundamentals.structural.exercises.bridge.abstraction.Bike;
import com.sda.fundamentals.structural.exercises.bridge.abstraction.Car;
import com.sda.fundamentals.structural.exercises.bridge.abstraction.Vehicle;
import com.sda.fundamentals.structural.exercises.bridge.implementation.Assemble;
import com.sda.fundamentals.structural.exercises.bridge.implementation.Produce;

public class BridgePattern {
    public static void main(String[] args) {
        Vehicle vehicle1 = new Car(new Produce(), new Assemble());
        vehicle1.manufacture();
        Vehicle vehicle2 = new Bike(new Produce(), new Assemble());
        vehicle2.manufacture();
    }
}