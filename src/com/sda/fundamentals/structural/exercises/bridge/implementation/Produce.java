package com.sda.fundamentals.structural.exercises.bridge.implementation;

public class Produce implements Workshop {
    @Override
    public void work() {
        System.out.print("Produced");
    }
}