package com.sda.fundamentals.structural.exercises.bridge.implementation;

public interface Workshop {
    void work();
}