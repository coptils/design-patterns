package com.sda.fundamentals.structural.exercises.bridge.implementation;

public class Assemble implements Workshop {
    @Override
    public void work() {
        System.out.print(" And");
        System.out.println(" Assembled.");
    }
}