package com.sda.fundamentals.structural.exercises.proxy;

public interface Image {
    void display();
}