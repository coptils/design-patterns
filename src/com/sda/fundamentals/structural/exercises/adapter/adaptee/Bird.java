package com.sda.fundamentals.structural.exercises.adapter.adaptee;

public interface Bird {
    void fly();

    void makeSound();
}