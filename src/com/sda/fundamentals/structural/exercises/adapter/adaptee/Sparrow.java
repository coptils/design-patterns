package com.sda.fundamentals.structural.exercises.adapter.adaptee;

// a concrete implementation of bird
public class Sparrow implements Bird {

    public void fly() {
        System.out.println("Flying");
    }

    public void makeSound() {
        System.out.println("Chirp Chirp");
    }
}