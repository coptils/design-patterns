package com.sda.fundamentals.structural.exercises.adapter.target;

public interface ToyDuck {
    // target interface
    // toyducks dont fly they just make
    // squeaking sound
    void squeak();
}
  