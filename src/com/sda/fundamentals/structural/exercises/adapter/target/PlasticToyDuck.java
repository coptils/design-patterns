package com.sda.fundamentals.structural.exercises.adapter.target;

public class PlasticToyDuck implements ToyDuck {
    public void squeak() {
        System.out.println("Squeak");
    }
}
  