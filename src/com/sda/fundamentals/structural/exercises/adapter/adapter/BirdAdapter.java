package com.sda.fundamentals.structural.exercises.adapter.adapter;

import com.sda.fundamentals.structural.exercises.adapter.adaptee.Bird;
import com.sda.fundamentals.structural.exercises.adapter.target.ToyDuck;

public class BirdAdapter implements ToyDuck {
    // You need to implement the interface your
    // client expects to use.
    Bird bird;

    public BirdAdapter(Bird bird) {
        // we need reference to the object we
        // are adapting
        this.bird = bird;
    }

    public void squeak() {
        // translate the methods appropriately
        bird.makeSound();
    }
}