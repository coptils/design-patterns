package com.sda.fundamentals.behavioral.chainofresponsability;

import com.sda.fundamentals.behavioral.chainofresponsability.handlers.AbstractSupportHandler;
import com.sda.fundamentals.behavioral.chainofresponsability.handlers.BillingSupportHandler;
import com.sda.fundamentals.behavioral.chainofresponsability.handlers.GeneralSupportHandler;
import com.sda.fundamentals.behavioral.chainofresponsability.handlers.TechnicalSupportHandler;

// Client ul- care initiaza requestul pe care unul dintre handlere il poate manipula
public class RequestorClient {
    public static AbstractSupportHandler getHandlerChain() {
        AbstractSupportHandler technicalSupportHandler = new TechnicalSupportHandler(AbstractSupportHandler.TECHNICAL);
        AbstractSupportHandler billingSupportHandler = new BillingSupportHandler(AbstractSupportHandler.BILLING);
        AbstractSupportHandler generalSupportHandler = new GeneralSupportHandler(AbstractSupportHandler.GENERAL);

        technicalSupportHandler.setNextHandler(billingSupportHandler);
        billingSupportHandler.setNextHandler(generalSupportHandler);

        return technicalSupportHandler;
    }
}