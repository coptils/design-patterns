package com.sda.fundamentals.behavioral.chainofresponsability.exercise.model;

public class Number {
    private final int number;

    public Number(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

}