package com.sda.fundamentals.behavioral.chainofresponsability.exercise.handlers;

import com.sda.fundamentals.behavioral.chainofresponsability.exercise.model.Number;

public class NegativeProcessor extends Processor {
    public NegativeProcessor(Processor processor) {
        super(processor);

    }

    public void process(Number request) {
        if (request.getNumber() < 0) {
            System.out.println("NegativeProcessor : " + request.getNumber());
        } else {
            super.process(request);
        }
    }
} 