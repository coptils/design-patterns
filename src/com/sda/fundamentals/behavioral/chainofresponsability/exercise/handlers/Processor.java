package com.sda.fundamentals.behavioral.chainofresponsability.exercise.handlers;

import com.sda.fundamentals.behavioral.chainofresponsability.exercise.model.Number;

public abstract class Processor {
    private final Processor processor;

    public Processor(Processor processor) {
        this.processor = processor;
    }

    public void process(Number request) {
        if (processor != null)
            processor.process(request);
    }

}