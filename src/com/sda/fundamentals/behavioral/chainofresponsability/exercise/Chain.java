package com.sda.fundamentals.behavioral.chainofresponsability.exercise;

import com.sda.fundamentals.behavioral.chainofresponsability.exercise.handlers.NegativeProcessor;
import com.sda.fundamentals.behavioral.chainofresponsability.exercise.handlers.PositiveProcessor;
import com.sda.fundamentals.behavioral.chainofresponsability.exercise.handlers.Processor;
import com.sda.fundamentals.behavioral.chainofresponsability.exercise.handlers.ZeroProcessor;
import com.sda.fundamentals.behavioral.chainofresponsability.exercise.model.Number;

public class Chain {
    Processor chain;

    public Chain() {
        buildChain();
    }

    private void buildChain() {
        chain = new NegativeProcessor(new ZeroProcessor(new PositiveProcessor(null)));
    }

    public void process(Number request) {
        chain.process(request);
    }

}