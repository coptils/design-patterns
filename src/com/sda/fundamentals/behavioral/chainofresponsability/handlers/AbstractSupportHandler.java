package com.sda.fundamentals.behavioral.chainofresponsability.handlers;

// Handler -> clasa de baza abstracta, actioneaza ca si o interfata pentru a manipula requestul
public abstract class AbstractSupportHandler {

    // se asigneaza request-ului un nivel de solutionare
    public static int TECHNICAL = 10;
    public static int BILLING = 20;
    public static int GENERAL = 30;

    protected int level;

    protected AbstractSupportHandler nextHandler;

    public void setNextHandler(AbstractSupportHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public void receiveRequest(int level, String message) {
        if (this.level <= level) {
            handleRequest(message);
        }
        if (nextHandler != null) {
            nextHandler.receiveRequest(level, message);
        }
    }

    protected abstract void handleRequest(String message);

}