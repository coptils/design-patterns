package com.sda.fundamentals.behavioral.chainofresponsability.handlers;

// Handle Concret -> fie face ceva cu requestul, fie il paseaza mai departe
public class BillingSupportHandler extends AbstractSupportHandler {

    public BillingSupportHandler(int level) {
        this.level = level;
    }

    @Override
    protected void handleRequest(String message) {
        System.out.println("BillingSupportHandler: Processing request. " + message);
    }
}