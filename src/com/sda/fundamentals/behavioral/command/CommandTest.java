package com.sda.fundamentals.behavioral.command;

import com.sda.fundamentals.behavioral.command.controller.Controler;
import com.sda.fundamentals.behavioral.command.sort.*;

// clientul
public class CommandTest {

    public static void main(String[] args) {
        int[] arr = generateRandomArray(10_000);

        Controler controler = new Controler();

        // creem o comanda concreta
        CommandInterface bubbleSort = new BubbleSort(arr.clone()); // shallow copy of the original array
        controler.setCommand(bubbleSort);
        controler.runSort();

        CommandInterface quickSort = new QuickSort(arr.clone());
        controler.setCommand(quickSort);
        controler.runSort();

        CommandInterface bucketSort = new BucketSort(arr.clone());
        controler.setCommand(bucketSort);
        controler.runSort();

        CommandInterface mergeSort = new MergeSort(arr.clone());
        controler.setCommand(mergeSort);
        controler.runSort();

        CommandInterface heapSort = new HeapSort(arr.clone());
        controler.setCommand(heapSort);
        controler.runSort();

    }

    private static int[] generateRandomArray(int size) {
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            int n = (int) (Math.random() * 20 + 1);
            arr[i] = n;
        }
        return arr;
    }

}
