package com.sda.fundamentals.behavioral.command.controller;

import com.sda.fundamentals.behavioral.command.sort.CommandInterface;

// receiver - efectueaza actiunea in functie de comanda primita
public class Controler {

    // has-a aggregation => composition
    private CommandInterface command;

    public void setCommand(CommandInterface command) {
        this.command = command;
    }

    public void runSort() {
        command.execute();
    }

}
