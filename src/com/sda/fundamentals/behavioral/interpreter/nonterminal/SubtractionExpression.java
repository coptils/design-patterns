package com.sda.fundamentals.behavioral.interpreter.nonterminal;

import com.sda.fundamentals.behavioral.interpreter.Expression;

public class SubtractionExpression implements Expression {
    private final Expression firstExpression;
    private final Expression secondExpression;

    public SubtractionExpression(Expression firstExpression, Expression secondExpression) {
        this.firstExpression = firstExpression;
        this.secondExpression = secondExpression;
    }

    @Override
    public int interpret() {
        return this.firstExpression.interpret() - this.secondExpression.interpret();
    }

    @Override
    public String toString() {
        return "-";
    }
}