package com.sda.fundamentals.behavioral.interpreter.terminal;

import com.sda.fundamentals.behavioral.interpreter.Expression;

public class NumberExpression implements Expression {
    private final int number;

    public NumberExpression(int number) {
        this.number = number;
    }

    public NumberExpression(String number) {
        this.number = Integer.parseInt(number);
    }

    @Override
    public int interpret() {
        return this.number;
    }
}
