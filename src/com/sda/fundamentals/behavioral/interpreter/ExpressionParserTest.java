package com.sda.fundamentals.behavioral.interpreter;

// Interpretor => implementeaza un limbaj specializat
public class ExpressionParserTest {

    public static void main(String[] args) {
        // POSTFIX mathematical expression
        // 2
        // 1
        // 5
        // +
        // pop => 5
        // pop => 1
        // push (5 + 1)
        // 2
        // 6
        // *
        // pop 6
        // pop 2
        String input = "2 1 5 + *";  // (5 + 1) * 2 = 6 * 2 = 12
        ExpressionParser expressionParser = new ExpressionParser();
        int result = expressionParser.parse(input);
        System.out.println("Final result: " + result);
    }

}