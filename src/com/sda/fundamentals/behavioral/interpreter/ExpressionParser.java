package com.sda.fundamentals.behavioral.interpreter;

import com.sda.fundamentals.behavioral.interpreter.nonterminal.AdditionExpression;
import com.sda.fundamentals.behavioral.interpreter.nonterminal.MultiplicationExpression;
import com.sda.fundamentals.behavioral.interpreter.nonterminal.SubtractionExpression;
import com.sda.fundamentals.behavioral.interpreter.terminal.NumberExpression;

import java.util.Stack;

public class ExpressionParser {

    Stack stack = new Stack<>(); // stiva => FILO

    public int parse(String str) {
        String[] tokenList = str.split(" ");

        for (String symbol : tokenList) {
            if (!isOperator(symbol)) {
                Expression numberExpression = new NumberExpression(symbol);
                stack.push(numberExpression);
                System.out.println(String.format("Pushed to stack: %d", numberExpression.interpret()));
            } else if (isOperator(symbol)) {
                Expression firstExpression = (Expression) stack.pop();
                Expression secondExpression = (Expression) stack.pop();
                System.out.println(String.format("Popped operands %d and %d",
                        firstExpression.interpret(), secondExpression.interpret()));
                Expression operator = getExpressionObject(firstExpression, secondExpression, symbol);
                System.out.println(String.format("Applying Operator: %s", operator));
                int result = operator.interpret();
                NumberExpression resultExpression = new NumberExpression(result);
                stack.push(resultExpression);
                System.out.println(String.format("Pushed result to stack: %d", resultExpression.interpret()));
            }
        }

        int result = ((Expression) stack.pop()).interpret();
        return result;
    }

    private static boolean isOperator(String symbol) {
        return (symbol.equals("+")
                || symbol.equals("-")
                || symbol.equals("*"));
    }

    private static Expression getExpressionObject(Expression firstExpression, Expression secondExpression, String symbol) {
        if (symbol.equals("+")) {
            return new AdditionExpression(firstExpression, secondExpression);
        } else if (symbol.equals("-")) {
            return new SubtractionExpression(firstExpression, secondExpression);
        } else {
            return new MultiplicationExpression(firstExpression, secondExpression);
        }
    }

}