package com.sda.fundamentals.behavioral.interpreter;

public interface Expression {
    int interpret();
}