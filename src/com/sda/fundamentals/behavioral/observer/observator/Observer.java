package com.sda.fundamentals.behavioral.observer.observator;

import java.math.BigDecimal;

// implementat de obiectele care trebuie notificate in legatura cu schimbarea pretului produsului
public interface Observer {
    void update(Observer observer, String productName, BigDecimal bidAmount);
}