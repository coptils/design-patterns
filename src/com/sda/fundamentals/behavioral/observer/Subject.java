package com.sda.fundamentals.behavioral.observer;

import com.sda.fundamentals.behavioral.observer.observator.Observer;

import java.math.BigDecimal;

// ataseaza sau inlatura obiectele de tipul observer
public interface Subject {
    void registerObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObservers();

    void setBidAmount(Observer observer, BigDecimal newBidAmount);
}