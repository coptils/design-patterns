package com.sda.fundamentals.behavioral.templatemethod;

public abstract class Pizza {

    public final void bakingPizza() {
        System.out.println("Start baking pizza...");
        formingDough();
        addIngredients();
        bakingDough();
        System.out.println("Pizza is baked.");
    }

    private void formingDough() {
        System.out.println("Forming dough");
    }

    private void bakingDough() {
        System.out.println("Baking dough");
    }

    protected abstract void addIngredients();
}
