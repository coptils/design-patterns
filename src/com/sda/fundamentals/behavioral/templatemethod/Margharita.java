package com.sda.fundamentals.behavioral.templatemethod;

public class Margharita extends Pizza {

    @Override
    protected void addIngredients() {
        System.out.println("Add Margharita ingredients: Tomato Sauce, Chees");
    }

}
