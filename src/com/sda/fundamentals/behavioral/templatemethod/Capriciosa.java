package com.sda.fundamentals.behavioral.templatemethod;

public class Capriciosa extends Pizza {

    @Override
    protected void addIngredients() {
        System.out.println("Add Capriciosa ingredients: Tomato Sauce, Cheese, Ham, Mushrooms");
    }

}
