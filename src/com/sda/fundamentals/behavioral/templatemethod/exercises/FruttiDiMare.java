package com.sda.fundamentals.behavioral.templatemethod.exercises;

import com.sda.fundamentals.behavioral.templatemethod.Pizza;

public class FruttiDiMare extends Pizza {

    @Override
    protected void addIngredients() {
        System.out.println("Frutti di Mare");
        System.out.println("Add ingredients: Tomato Sauce, Chees, Seafood");
    }
}
