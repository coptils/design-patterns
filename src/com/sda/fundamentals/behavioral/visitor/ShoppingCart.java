package com.sda.fundamentals.behavioral.visitor;

import com.sda.fundamentals.behavioral.visitor.item.Animal;
import com.sda.fundamentals.behavioral.visitor.item.Car;
import com.sda.fundamentals.behavioral.visitor.item.Computer;

public interface ShoppingCart {

    int visit(Car car);

    int visit(Computer computer);

    int visit(Animal animal);
}
