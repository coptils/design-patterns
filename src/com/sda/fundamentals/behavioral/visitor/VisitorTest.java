package com.sda.fundamentals.behavioral.visitor;

import com.sda.fundamentals.behavioral.visitor.item.Animal;
import com.sda.fundamentals.behavioral.visitor.item.Car;
import com.sda.fundamentals.behavioral.visitor.item.Computer;
import com.sda.fundamentals.behavioral.visitor.item.Item;

import java.util.ArrayList;

// Visitor -> decupleaza operatiile de structura obiectelor
public class VisitorTest {

    private static ShoppingCart visitor = new ShoppingCartImpl();

    public static void main(String[] args) {
        ArrayList<Item> items = new ArrayList<>();
        items.add(new Car("Ford Mondeo", "black", 14000));
        items.add(new Car("Opel Corsa", "white", 19000));
        items.add(new Computer("8 GB", "1 TB", "3,8 GHz 2 core", 2000));
        items.add(new Computer("16 GB", "1 TB SSD", "4,5 GHz 8 core", 5000));

        int total = items.stream()
                .map(item -> item.accept(visitor))
                .reduce(0, Integer::sum);

        System.out.println("Total Cost of items = " + total);

        // exercises
        items = new ArrayList<>();
        items.add(new Animal("Dog", 140));
        items.add(new Animal("Cat", 58));

        visitor = new ShoppingCartImpl();

        total = items.stream()
                .map(item -> item.accept(visitor))
                .reduce(0, (currentValue, sum) -> Integer.sum(currentValue, sum));

        System.out.println("Total Cost of Animal items = " + total);
    }

}
