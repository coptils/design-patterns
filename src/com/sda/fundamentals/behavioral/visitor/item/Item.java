package com.sda.fundamentals.behavioral.visitor.item;

import com.sda.fundamentals.behavioral.visitor.ShoppingCart;

public interface Item {

    int accept(ShoppingCart visitor);
}
