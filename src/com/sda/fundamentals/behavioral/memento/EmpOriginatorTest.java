package com.sda.fundamentals.behavioral.memento;

// Memento - ofera abilitatea de a face restore unui obiect la starea lui anterioara
public class EmpOriginatorTest {

    public static void main(String[] args) {
        EmpOriginator empOriginator = new EmpOriginator(306, "Mark Ferguson", "131011789610", "Sales Manager");

        EmpMemento empMemento = empOriginator.saveToMemento();

        EmpCaretaker empCaretaker = new EmpCaretaker();

        empCaretaker.addMemento(empMemento);
        System.out.println("\nOriginal EmpOriginator");
        empOriginator.printInfo();

        System.out.println("\nEmpOriginator after updating phone number");
        empOriginator.setEmpPhoneNo("131011888886");
        empMemento = empOriginator.saveToMemento();
        empCaretaker.addMemento(empMemento);
        empOriginator.printInfo();

        System.out.println("\nEmpOriginator after updating designation");
        empOriginator.setEmpDesignation("Senior Sales Manager");
        empMemento = empOriginator.saveToMemento();
        empCaretaker.addMemento(empMemento);
        empOriginator.printInfo();

        System.out.println("\nEmpOriginator after undoing designation update");
        empMemento = empCaretaker.getMemento();
        empOriginator.undoFromMemento(empMemento);
        empMemento = empCaretaker.getMemento();
        empOriginator.undoFromMemento(empMemento);
        empOriginator.printInfo();

        System.out.println("\nOriginal EmpOriginator after undoing phone number update");
        empMemento = empCaretaker.getMemento();
        empOriginator.undoFromMemento(empMemento);
        empOriginator.printInfo();
    }

}
