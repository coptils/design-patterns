package com.sda.fundamentals.behavioral.memento;

import java.util.ArrayDeque;
import java.util.Deque;

public class EmpCaretaker {
    final Deque<EmpMemento> mementos = new ArrayDeque<>();  // salvam starile intermediare cu ajutorul unei cozi

    public EmpMemento getMemento() {
        EmpMemento empMemento = mementos.pop();
        return empMemento;
    }

    public void addMemento(EmpMemento memento) {
        mementos.push(memento);
    }
}