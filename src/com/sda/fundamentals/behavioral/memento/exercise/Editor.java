package com.sda.fundamentals.behavioral.memento.exercise;

public class Editor {
    //state
    private String editorContents;

    public void setState(String contents) {
        this.editorContents = contents;
    }

    public EditorMemento save() {
        return new EditorMemento(editorContents);
    }

    public void restoreToState(EditorMemento memento) {
        editorContents = memento.getSavedState();
    }

    @Override
    public String toString() {
        return "Editor{" +
                "editorContents='" + editorContents + '\'' +
                '}';
    }
}