package com.sda.fundamentals.behavioral.memento.exercise;

public class Main {
    public static void main(String[] args) {
        Editor editor = new Editor();
        editor.setState("original content");

        EditorMemento memento = editor.save();

        Caretaker caretaker = new Caretaker();
        caretaker.addMemento(memento);

        System.out.println("Original Editor");

        System.out.println(editor);

        System.out.println("\nEditor after updating the initial state");
        editor.setState("modified content - first time");
        memento = editor.save();
        caretaker.addMemento(memento);

        System.out.println(editor);

        System.out.println("\nEditor after updating the previously modified state");
        editor.setState("modified content - second time");
        memento = editor.save();
        caretaker.addMemento(memento);

        System.out.println(editor);


        System.out.println("\nEditor after undoing \"second time\" update");
        memento = caretaker.getMemento();
        editor.restoreToState(memento);
        memento = caretaker.getMemento();
        editor.restoreToState(memento);

        System.out.println(editor);

        System.out.println("\nOriginal Editor after undoing \"first time\" update");
        memento = caretaker.getMemento();
        editor.restoreToState(memento);

        System.out.println(editor);
    }
}
