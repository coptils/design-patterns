package com.sda.fundamentals.behavioral.memento.exercise;

import java.util.Stack;

public class Caretaker {
    final Stack<EditorMemento> mementos = new Stack<>();

    public EditorMemento getMemento() {
        EditorMemento memento = mementos.pop();
        return memento;
    }

    public void addMemento(EditorMemento memento) {
        mementos.push(memento);
    }
}
