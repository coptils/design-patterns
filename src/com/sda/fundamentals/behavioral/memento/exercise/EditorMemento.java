package com.sda.fundamentals.behavioral.memento.exercise;

public class EditorMemento {
    private final String editorState;

    public EditorMemento(String state) {
        editorState = state;
    }

    public String getSavedState() {
        return editorState;
    }
}