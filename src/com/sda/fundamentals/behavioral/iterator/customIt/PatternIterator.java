package com.sda.fundamentals.behavioral.iterator.customIt;

public interface PatternIterator {
    DesignPattern nextPattern();

    boolean isLastPattern();
}