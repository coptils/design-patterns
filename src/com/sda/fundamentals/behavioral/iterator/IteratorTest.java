package com.sda.fundamentals.behavioral.iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorTest {
    public static void main(String[] args) {
        ArrayList<String> cars = new ArrayList<>();
//        List<String> cars = new ArrayList<>();
        cars.add("Volvo");
        cars.add("BMW");
        cars.add("Ford");
        cars.add("Mazda");

        // Get the iterator
        Iterator<String> iterator = cars.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next()); // iterator.next() <=> cars.get(index);
        }

        System.out.println("--------------------------");

    }

}
