package com.sda.fundamentals.behavioral.state.exercise;

public class Silent implements MobileAlertState {
    @Override
    public void alert(AlertStateContext ctx) {
        System.out.println("silent...");
    }

}