package com.sda.fundamentals.behavioral.state.exercise;

public interface MobileAlertState {
    void alert(AlertStateContext ctx);
}