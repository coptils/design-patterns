package com.sda.fundamentals.behavioral.state.exercise;

public class Vibration implements MobileAlertState {
    @Override
    public void alert(AlertStateContext ctx) {
        System.out.println("vibration...");
    }

}