package com.sda.fundamentals.behavioral.state;

public class TestStatePattern {
    public static void main(String[] args) {
        StatePattern.FanWallControl fanWallControl = new StatePattern().new FanWallControl();
        fanWallControl.set_state(new StatePattern.SpeedLevel1());
        fanWallControl.rotate();
        System.out.println(fanWallControl.toString());
    }
}
