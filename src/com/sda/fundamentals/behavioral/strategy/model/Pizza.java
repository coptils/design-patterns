package com.sda.fundamentals.behavioral.strategy.model;

import com.sda.fundamentals.behavioral.strategy.paymentstrategies.Payment;

import java.util.ArrayList;

public class Pizza {

    private ArrayList<Ingredient> ingredients;
    private double amount;

    public Pizza() {
        this.amount = 10.0;
        this.ingredients = new ArrayList<>();
    }

    public void addIngredient(Ingredient ingredient) {
        this.ingredients.add(ingredient);
    }

    public void removeIngredient(Ingredient ingredient) {
        this.ingredients.remove(ingredient);
    }

    public double calculateTotal() {
        double sum = 0;
        sum = ingredients.stream()
                .map(Ingredient::getPrice)
                .reduce(sum, (price, intermediateSum) -> Double.sum(price, intermediateSum));
        return sum;
    }

    // aceasta metoda realizeaza legatura cu Payment strategies
    public void pay(Payment payment) {
        payment.pay(amount + calculateTotal());
    }
}
