package com.sda.fundamentals.behavioral.strategy.paymentstrategies;

public interface Payment {
    void pay(double amount);
}
