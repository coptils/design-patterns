package com.sda.fundamentals.behavioral.strategy.paymentstrategies;

public class BtPay implements Payment {

    private String pass;

    public BtPay(String pass){
        this.pass = pass;
    }

    @Override
    public void pay(double amount) {
        System.out.println("Cost: " + amount + "$, paid whit BTPay");
    }
}
