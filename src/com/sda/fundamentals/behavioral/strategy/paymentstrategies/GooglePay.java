package com.sda.fundamentals.behavioral.strategy.paymentstrategies;

public class GooglePay implements Payment {

    private String email;
    private String pass;

    public GooglePay(String email, String pass){
        this.email = email;
        this.pass = pass;
    }

    @Override
    public void pay(double amount) {
        System.out.println("Cost: " + (amount - (amount * 0.1)) + "$, paid whit GooglePay");
    }
}
