package com.sda.fundamentals.behavioral.strategy.paymentstrategies;

import java.time.LocalDate;

public class CreditCard implements Payment {

    private String fullName;
    private String cardNumber;
    private String cvv;
    private LocalDate dateOfExpiry;

    public CreditCard(String fullName, String cardNumber, String cvv, LocalDate dateOfExpiry) {
        this.fullName = fullName;
        this.cardNumber = cardNumber;
        this.cvv = cvv;
        this.dateOfExpiry = dateOfExpiry;
    }

    @Override
    public void pay(double amount) {
        System.out.println("Cost: " + amount + "$, paid whit credit card");
    }

}
