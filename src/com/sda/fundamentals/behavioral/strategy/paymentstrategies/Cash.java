package com.sda.fundamentals.behavioral.strategy.paymentstrategies;

public class Cash implements Payment {

    public Cash() {

    }

    @Override
    public void pay(double amount) {
        System.out.println("Cost: " + amount + "$, paid whit cash");
    }

}
