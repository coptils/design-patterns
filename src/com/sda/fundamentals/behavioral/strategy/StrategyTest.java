package com.sda.fundamentals.behavioral.strategy;

import com.sda.fundamentals.behavioral.strategy.model.Ingredient;
import com.sda.fundamentals.behavioral.strategy.model.Pizza;
import com.sda.fundamentals.behavioral.strategy.paymentstrategies.BtPay;
import com.sda.fundamentals.behavioral.strategy.paymentstrategies.Cash;
import com.sda.fundamentals.behavioral.strategy.paymentstrategies.CreditCard;
import com.sda.fundamentals.behavioral.strategy.paymentstrategies.GooglePay;

import java.time.LocalDate;

public class StrategyTest {
    public static void main(String[] args) {
        Pizza pizza = new Pizza();

        Ingredient ingredient1 = new Ingredient("Chees",15.0);
        Ingredient ingredient2 = new Ingredient("Ham",20.0);
        Ingredient ingredient3 = new Ingredient("Mushrooms",10.0);

        pizza.addIngredient(ingredient1);
        pizza.addIngredient(ingredient2);
        pizza.addIngredient(ingredient3);

        pizza.pay(new Cash());

        pizza.pay(new CreditCard("David Wilson", "5195552421627633", "324", LocalDate.of(2025, 1, 1)));

        pizza.pay(new GooglePay("sda@sdacademy.pl", "password"));

        pizza.pay(new BtPay( "password"));
    }

}
