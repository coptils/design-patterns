package com.sda.fundamentals.reflection;

import com.sda.fundamentals.creational.abstractF.pizza.Capriciosa;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Reflection {
    public static void main(String[] args) throws Exception {
//        Car car = new Car();
//        car.getType();
        // call getType method from Car class by using reflection
        Class<?> clazz = Class.forName("com.sda.fundamentals.reflection.car.Car");

        Method isGetTypeMethod = clazz.getDeclaredMethod("getType");

        System.out.println("Car type is: " + isGetTypeMethod.invoke(clazz.getDeclaredConstructor().newInstance()));


        // capriciosa class fields
        Capriciosa capriciosa = new Capriciosa(500);
        System.out.println("capriciosa instance is of type:" + capriciosa.getClass().getSimpleName());
        System.out.println("the superclass is:" + capriciosa.getClass().getSuperclass().getSimpleName());
        System.out.println("the package of the Capriciosa class is:" + capriciosa.getClass().getPackageName());

        Class<?> capriciosaClass = Class.forName("com.sda.fundamentals.creational.abstractF.pizza.Capriciosa");

        int goatMods = capriciosaClass.getModifiers();

        System.out.println("the modifier acces of capriciosa class is public? " + Modifier.isPrivate(goatMods));
    }
}
